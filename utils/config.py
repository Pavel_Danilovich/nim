import os


class Config:
    BOT_API_TOKEN = os.environ.get('BOT_API_TOKEN')

    REDIS_HOST = os.environ.get('REDIS_HOST')
    REDIS_PORT = os.environ.get('REDIS_PORT')
    REDIS_LOGIN = os.environ.get('REDIS_LOGIN')
    REDIS_PASSWORD = os.environ.get('REDIS_PASSWORD')

