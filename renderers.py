from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.callback_data import CallbackData

cross_zero_cb = CallbackData('cross_zero', 'action', 'data')


def render_cross_zero(game):
    keyboard = InlineKeyboardMarkup()
    for i, row in enumerate(game.field):
        for j, col in enumerate(row):
            keyboard.insert(InlineKeyboardButton(col, callback_data=cross_zero_cb.new(
                action='player_step', data=f'{game.player_mark}${i}${j}')))
    return keyboard
