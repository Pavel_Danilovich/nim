import math


class CrossZero:
    # TODO: Add support difference nums for win condition
    win_condition = 3

    def __init__(self, size, player_mark, field=None):
        assert size % 2 != 0

        self.size = size
        self.field = field
        self.cross_count = 0
        self.zero_count = 0
        self.player_mark = player_mark

    @classmethod
    def create_game(cls, size, player_mark, field=None):
        if player_mark == 'o':
            return PlayerZero(size, player_mark, field=None)

    def generate_field(self):
        if self.size * self.size > 100:
            raise Exception('Error: field bigger than possible')
        self.field = [['#'] * self.size for _ in range(self.size)]

    def first_step(self):
        pass

    def next_step(self):
        pass

    def player_step(self, i, j):
        self.field[i][j] = self.player_mark

    @property
    def middle_point(self):
        return math.ceil(self.size / 2) - 1


class PlayerZero(CrossZero):
    def __init__(self, size, player_mark, field=None):
        super().__init__(size, player_mark, field=None)
        self.action = 'o'

    def first_step(self):
        mp = self.middle_point
        self.field[mp][mp] = 'x'
        self.cross_count += 1

    def next_step(self):
        self.field[0][0] = 'x'
        self.cross_count += 1

    def player_step(self, i, j):
        super().player_step(i, j)
        self.zero_count += 1
