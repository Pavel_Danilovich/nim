import logging
import time
from pathlib import Path

from aiogram import Dispatcher
from aiogram.bot import Bot
from aiogram.contrib.fsm_storage.redis import RedisStorage2
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import InputFile, ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils import executor

from utils.config import Config
import models
from renderers import render_cross_zero, cross_zero_cb


bot = Bot(token=Config.BOT_API_TOKEN, parse_mode='HTML')
storage = RedisStorage2(host=Config.REDIS_HOST, port=Config.REDIS_PORT, db=0)
logging.info('Start Redis as bot storage')
dp = Dispatcher(bot=bot, storage=storage)

logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands=['start'], state='*')
async def handle_start_message(message, state):
    path_to_img = str(Path('static', 'img', 'elsa.jpg'))
    input_file = InputFile(path_to_img)
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True).add(
        KeyboardButton('Ним'),
        KeyboardButton('Крестики-нолики')
    )
    text = 'В какую игру хочешь сыграть?'
    await message.answer_photo(photo=input_file, reply_markup=keyboard, caption=text)


@dp.message_handler(lambda message: message.text == 'Крестики-нолики', state='*')
async def handle_start_cross_zero(message, state):
    keyboard = InlineKeyboardMarkup()
    buttons = [InlineKeyboardButton(f'{num}x{num}', callback_data=cross_zero_cb.new(
        action='select_field_size', data=num)) for num in (3, 5, 7)]
    keyboard.add(
        *buttons
    )
    # TODO: add support eng
    text = 'Выбери размер поля'
    await message.answer(text, reply_markup=keyboard)


@dp.callback_query_handler(cross_zero_cb.filter(action='select_field_size'), state='*')
async def handle_select_field_size(query, callback_data, state):
    size = int(callback_data['data'])
    async with state.proxy() as data:
        data['size'] = size
    keyboard = InlineKeyboardMarkup()
    buttons = [InlineKeyboardButton(sym, callback_data=cross_zero_cb.new(
        action='select_mark', data=sym)) for sym in ['x', 'o']]
    keyboard.add(
        *buttons
    )
    text = 'Какими будешь играть?'
    await query.message.edit_text(text, reply_markup=keyboard)


@dp.callback_query_handler(cross_zero_cb.filter(action='select_mark'), state='*')
async def handle_select_mark(query, callback_data, state):
    async with state.proxy() as data:
        size = data['size']
    mark = callback_data['data']
    game = models.PlayerZero.create_game(size, mark)
    game.generate_field()
    keyboard = render_cross_zero(game)

    await query.message.edit_text('Начинаем...', reply_markup=keyboard)
    game.first_step()
    time.sleep(2)
    async with state.proxy() as data:
        data['game'] = game.__dict__
    keyboard = render_cross_zero(game)
    await query.message.edit_text('Твой ход...', reply_markup=keyboard)


@dp.callback_query_handler(cross_zero_cb.filter(action='player_step'), state='*')
async def handle_player_step(query, callback_data, state):
    async with state.proxy() as data:
        game_data = data['game']
    game = models.CrossZero.create_game()


@dp.message_handler(state='*')
async def handle_reply_message(message, state):
    logging.info(message.text)
    # TODO: replace to inline keyboard
    keyboard = ReplyKeyboardMarkup().add(
        KeyboardButton('Ним'),
        KeyboardButton('Крестики-нолики')
    )
    await message.answer('В какую игру хочешь сыграть?', reply_markup=keyboard)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)



